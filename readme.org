
*DisKort* - extension for browser which decreases Discord elements.

Currently it's in beta, more of design was merged into css.

[[https://addons.mozilla.org/en-US/firefox/addon/diskort/][Add to Firefox]]

* Installation

** Firefox

Firefox < 103: Highly recommend to enable backdrop-filter for acrylic.

1. Go to ~about:config~
2. Set ~layout.css.backdrop-filter.enabled~ and ~gfx.webrender.all~ to true.
3. Reload Discord.

Debug mode:

1. Download [[https://gitdab.com/er2/diskort/archive/dev.zip][dev tag archive]] and unpack it.
2. Copy btfl.css from [[https://raw.githubusercontent.com/Er2ch/diskort/main/btfl.css][GitHub]] or compile own.
3. Go to ~about:debugging#/runtime/this-firefox~
3. Click on *Load Temponary Add-on...*
4. Choose any file in extension folder.

** Chrome, etc.

Release mode:

1. Go to ~chrome://extensions/~
2. Enable ~Developer mode~.
3. Create own (~Pack extension~) or download [[https://gitdab.com/er2/diskort/releases][from releases]].
4. Drag and drop ~diskort.crx~ to browser.

Debug mode:

1. Download [[https://gitdab.com/er2/diskort/archive/dev.zip][dev tag archive]] and unpack it.
2. Copy btfl.css from [[https://raw.githubusercontent.com/Er2ch/diskort/main/btfl.css][GitHub]] or compile own.
3. Go to ~chrome://extensions/~
3. Enable ~Developer mode~.
4. Click on ~Load unpacked~ and choose extension folder.
