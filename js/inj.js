/** DisKort
 * (c) Er2 2022
 * Zlib License
 */

const base = "https://raw.githubusercontent.com/Er2ch/diskort/main/";

async function inject() {
  try {
    var resp = await fetch(base + 'btfl.css');
    var text = await resp.text();
    var s = document.createElement("style");
    s.innerHTML = text;
    document.head.appendChild(s);
  } catch(e) { console.error(e); }
}
inject();
