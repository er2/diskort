/** DisKort
 * (c) Er2 2022
 * Zlib License
 */

// Disable CSP
chrome.webRequest.onHeadersReceived.addListener(
  function(det) {
    for (let head of det.responseHeaders) {
      let n = head.name.toLowerCase();
      if (n == 'content-security-policy' || n == 'content-security-policy-report-only'
      ) head.value = '';
    }
    return { responseHeaders: det.responseHeaders };
  },
  {urls: ['<all_urls>']}, ['blocking', 'responseHeaders']
);

chrome.webRequest.onBeforeSendHeaders.addListener(
  function(det) {
    for (let head of det.requestHeaders) {
      let n = head.name.toLowerCase();
      if (n == 'user-agent')
        head.value = 'DisKort/1.0 (Linux) Firefox/99.0';
    }
    return { requestHeaders: det.requestHeaders };
  },
  {urls: ['<all_urls>']}, ['blocking', 'requestHeaders']
);

